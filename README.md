Kirb(y) (knock-off)
================

This is a Kirb(y) knock-off game made for the VJ (Videogames) subject at Facultat d'Informàtica de Barcelona, Universitat Politècnica de Barcelona (FIB, UPC).

Authors
--------

* Alec L.
* Laura C.


About Kirb(y)
--------
Kirby is a registered franchise property of Nintendo and HAL Laboratory. We do not own the sprites, name or anything related to it.


License
--------
This project is under the MIT license.

